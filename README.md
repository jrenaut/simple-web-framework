# Simple Web Framework

This is a very simple web framework that will never be ready to use in production

## Description

I just wanted to write a web framework almost from scratch. It seemed like a fun thing to do. Since almost all my web development experience is in Django, this will probably work a lot like Django does except not nearly as well.

## Getting Started

### Dependencies

Check req.txt. Plan is for as few external dependencies as possible.

