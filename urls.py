from views import *

patterns = {"/": home, "hello": hello, "main": main}


def parse_url(url):
    parts = [p for p in url.split("/") if len(p) > 0]
    view = patterns.get(parts[0] if len(parts) > 0 else "/", None)
    if view:
        return view(parts[1:])
    return None, None
