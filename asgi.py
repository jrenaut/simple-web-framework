import uvicorn
from app import app

if __name__ == "__main__":
    config = uvicorn.Config("asgi:app", port=8081, log_level="info")
    server = uvicorn.Server(config)
    server.run()
