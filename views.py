def home(url_parts):
    return f"home", b"text/plain"


def hello(url_parts):
    return (
        f"<html><head><title>Hello World!</title></head><body><h2>Hello world!</h2></body></html>",
        b"text/html",
    )


def main(url_parts):
    return (
        "<html><head><title>Welcome to the website</title></head><body><h2>Welcome to the website</h2><p>We're glad you're here!</p><p>{}</p></body></html>".format(
            url_parts,
        ),
        b"text/html",
    )
