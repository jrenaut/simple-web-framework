from urls import parse_url


async def app(scope, receive, send):
    assert scope["type"] == "http"
    body = None
    content_type = None
    print(scope)
    body, content_type = parse_url(scope["path"])
    if not body:
        body = f"Url not found"
    if not content_type:
        content_type = b"text/plain"
    await send(
        {
            "type": "http.response.start",
            "status": 200,
            "headers": [
                [b"content-type", content_type],
            ],
        }
    )
    await send(
        {
            "type": "http.response.body",
            "body": body.encode("utf-8"),
        }
    )
